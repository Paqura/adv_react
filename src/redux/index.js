import { createStore, applyMiddleware } from 'redux';
import reducer from './reducer';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import history from '../history';
import createSagaMiddleware  from 'redux-saga';
import { saga } from '../ducks/people';

const sagaMiddleware = createSagaMiddleware();
const enhancer = applyMiddleware(sagaMiddleware, routerMiddleware(history), thunk, logger);
const store = createStore(reducer, enhancer);

sagaMiddleware.run(saga)

window.store = store;

export default store;