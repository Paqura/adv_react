import React from "react";
import { Link } from 'react-router-dom';

function UnAuthorized() {
  return (
    <div>
      <h2>
        Ты не авторизован, авторизуйся 
        <Link to="/auth/signin">здесь</Link>
      </h2>
    </div>
  );
}

export default UnAuthorized;
