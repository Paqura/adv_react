import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import ErrorField from '../common/ErrorField';
import emailValidator from 'email-validator';

class NewPersonForm extends Component {  
  render() { 
    const { handleSubmit } = this.props;
    return ( 
      <div>
        <form onSubmit={handleSubmit}>
           <Field name="firstName" component={ErrorField}/>
           <Field name="lastName" component={ErrorField}/>
           <Field name="email" component={ErrorField}/>
           <div>
             <input type="submit"/>
           </div>
        </form>
      </div>
     )
  }
}

const validate = ({email, firstName, lastName}) => {
  const errors = {};

  if(!email) errors.email = 'email is required';
  else if(!emailValidator.validate(email)) errors.email = 'invalid email';

  if(!firstName) errors.firstName = 'firstName is required';
  else if(firstName.length < 6) errors.firstName = 'short...';

  if(!lastName) errors.lastName = 'lastName is required';
  else if(lastName.length < 6) errors.lastName = 'short...';

  return errors;
}
 
export default reduxForm ({
  form: "person",
  validate
})(NewPersonForm);