import React, {Component} from 'react';
import { Route } from 'react-router-dom';
import AdminPage from './routes/AdminPage';
import AuthPage from './routes/Auth';
import ProtectedRoute from './common/ProtectedRoute';
import PersonPage from './routes/PersonPage';
import '../config';

export default class Root extends Component {
  render() {
    return (
      <div>
         <ProtectedRoute path="/admin" component={AdminPage}/>
         <Route path="/auth" component={AuthPage}/>
         <Route path="/people" component={PersonPage} />
      </div>
    )
  }
}