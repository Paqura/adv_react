import React, { Component } from 'react';
import NewPersonForm from '../people/NewPersonForm';
import { connect } from 'react-redux';
import { addPerson } from '../../ducks/people';

class PersonPage extends Component {
  state = {}
  render() { 
    const { addPerson } = this.props;
    return (
      <div>
        <h2>Add new person</h2>
        <NewPersonForm onSubmit={addPerson}/>
      </div>
     )
  }
}

const mapDispatchToProps = ({
  addPerson
})
 
export default connect(null, mapDispatchToProps)(PersonPage);