import React, { Component } from "react";
import SignInForm from "../auth/SignInForm";
import SignUpForm from "../auth/SignUpForm";
import { Route, NavLink } from "react-router-dom";
import { navActive } from "./styled";
import { connect } from "react-redux";
import {signUp} from "../../ducks/auth";
import { moduleName } from "../../ducks/auth";
import Loader from "../common/Loader";

class AuthPage extends Component {

  hangleSignIn = values => console.log(values);
  
  hangleSignUp = ({email, password}) => this.props.signUp(email, password);

  render() {
    const { loading } = this.props;
    return (
      <div>
        <h1>Auth page</h1>
        <NavLink 
          to="/auth/signin" 
          activeStyle={navActive}
        >
          sign in
        </NavLink>
        <NavLink 
          to="/auth/signup" 
          activeStyle={navActive}
        >
          sign up
        </NavLink>
        {loading && <Loader />}
        <Route
          path="/auth/signin"
          render={() => <SignInForm onSubmit={this.hangleSignIn} />}
        />
        <Route
          path="/auth/signup"
          render={() => <SignUpForm onSubmit={this.hangleSignUp} />}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loading: state[moduleName].loading
});

const mapDispatchToProps = ({
  signUp
})

export default connect(mapStateToProps, mapDispatchToProps)(AuthPage);
