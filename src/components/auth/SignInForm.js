import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';

class SignInForm extends Component {
  submit(values) {
    console.log(values)
  }
   
  render() { 
    const { handleSubmit } = this.props;
    return ( 
      <div>
        <h2>Sign in</h2>
        <form onSubmit={ handleSubmit }>
           <div className='form-field'>
              <label>Email:</label>
              <Field name="email" component="input" />
           </div>
           <div className='form-field'>
              <label>Password:</label>
              <Field name="password" component="input" type="password"/>
           </div>
           <div className="form-field">
              <input type="submit"/>
           </div>
        </form>
      </div>
     )
  }
}
 
export default reduxForm({
  form: 'auth'  
})(SignInForm);