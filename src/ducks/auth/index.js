import firebase from 'firebase';
import { appName } from "../../config";
import { Record } from "immutable";

const ReducerShema = Record({
  user: null,
  error: null,
  loading: false
});

export const moduleName = "auth";

export const SIGN_UP_REQUEST = `${appName}/${moduleName}/SIGN_UP_REQUEST`;
export const SIGN_IN_SUCCESS = `${appName}/${moduleName}/SIGN_UP_SUCCESS`;
export const SIGN_UP_FAILURE = `${appName}/${moduleName}/SIGN_UP_FAILURE`;

export const signUp = (email, password) => dispatch => {
  console.log('123');
  dispatch({
    type: SIGN_UP_REQUEST
  });

  firebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .then(user =>
      dispatch({
        type: SIGN_IN_SUCCESS,
        payload: { user }
      })
    )
    .catch(err =>
      dispatch({
        type: SIGN_UP_FAILURE,
        err
      })
    );
};


export default function authReducer(state = new ReducerShema(), { type, payload, error }) {
  switch (type) {
    case SIGN_UP_REQUEST:
      return state.set("loading", true);
    case SIGN_IN_SUCCESS:
      return state
        .set("loading", false)
        .set("user", payload.user)
        .set("error", null);
    case SIGN_UP_FAILURE:
      return state
        .set("loading", false)
        .set("error", error);
    default:
      return state;
  }
}


