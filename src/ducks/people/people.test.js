import { addPersonSaga, ADD_PERSON, ADD_PERSON_REQUEST } from "./index";

import { generateID } from "../../utils";

import { call, put } from "redux-saga/effects";

it("должно задеспатчить данные с id", () => {
  const person = {
    firstName: "Slava",
    email: "test@mail.ru"
  };
  const saga = addPersonSaga({
    type: ADD_PERSON_REQUEST,
    payload: person
  });

  expect(saga.next().value).toEqual(call(generateID));

  const id = generateID();

  expect(saga.next(id).value).toEqual(
    put({
      type: ADD_PERSON,
      payload: { ...person, id }
    })
  );
});
