// import firebase from 'firebase';
import { appName } from "../../config";
import { Record, List } from "immutable";
import { call, takeEvery, put } from 'redux-saga/effects';
import { generateID } from '../../utils';

const ReducerState = Record({
  entities: new List([])
});

const PersonRecord = Record({
  id: null, 
  firstName: null,
  lastName: null,
  email: null
});

export const moduleName = "people";
const prefix = `${appName}/${moduleName}`;
export const ADD_PERSON_REQUEST = `${prefix}/ADD_PERSON_REQUEST`;
export const ADD_PERSON = `${prefix}/ADD_PERSON`;


export function addPerson(person) {
  return {
    type: ADD_PERSON_REQUEST,
    payload: person
  }
}


export const addPersonSaga = function* (action) {
  const id = yield call(generateID);
  console.log(action)
  yield put({
    type: ADD_PERSON,
    payload: {...action.payload, id}
  })
}

export const saga = function* () {
  yield takeEvery(ADD_PERSON_REQUEST, addPersonSaga);
}


export default function peopleReducer(state = new ReducerState(), { type, payload, error }) {
  switch (type) {
    case ADD_PERSON:
      return state.update('entities', entities => entities.push(new PersonRecord(payload)))
    default:
      return state;
  }
}


