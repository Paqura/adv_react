import firebase from 'firebase';

export const appName = "test-59673";

export const firebaseConfig = {
  apiKey: "AIzaSyAzVW5G4q30yqSfrAhYZLFaxq5oIipYgYs",
  authDomain: `${appName}.firebaseapp.com`,
  databaseURL: `https://${appName}.firebaseio.com`,
  projectId: appName,
  storageBucket: "",
  messagingSenderId: "773927898477"
};

firebase.initializeApp(firebaseConfig);